<?php
/**
 * Created by PhpStorm.
 * User: elad
 * Date: 28/06/18
 * Time: 11:05
 */

namespace Closeapp\JoinQuery;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use function is_string;

class Join
{
	/**
	 * @param Builder|Model|string $model
	 * @return Model
	 * @throws \Exception
	 */
	private static function getModel($model)
	{
		if ($model instanceof Builder) {
			return $model->getModel();
		}
		if (is_string($model)) {
			$model = new $model;
		}
		if ($model instanceof Model) {
			return $model;
		}
		trigger_error("expect to get Builder|Model|string");
		return $model;
	}

	/**
	 * @param Builder $baseQuery
	 * @param Builder|Model|string $joinClass
	 * @param array $attributes
	 * @param string $type
	 * @return Builder
	 * @throws \Exception
	 */
	public static function asBelongsTo(Builder $baseQuery, $joinClass, $attributes = [], $type = "left")
	{
		$mainModel = self::getModel($baseQuery);
		$mainTableName = $mainModel->getTable();
		$mainForeignKey = $mainModel->getForeignKey();

		$joinModel = self::getModel($joinClass);
		$joinTableName = $joinModel->getTable();
		$joinForeignKey = $joinModel->getForeignKey();

		if (!$mainForeignKey) {
			$mainForeignKey = "_" . $joinForeignKey;
		}
		if (!empty($attributes)) {
			$query = $joinModel->newQuery();
			if (!in_array($mainForeignKey, $attributes)) {
				$attributes[] = $mainModel->getKeyName() . " as " . $mainForeignKey;
			}
			$query->select(self::getAsJoinAttribute($attributes, $joinTableName));
			$asTableName = $joinTableName . Str::random(6);
			return self::joinQuery($baseQuery, $query, $asTableName, "$mainTableName." . $joinForeignKey, "=",
				"$asTableName." . $mainForeignKey, $type);
		} else {
			$baseQuery->join($joinTableName, "$mainTableName." . $joinForeignKey, "=",
				$joinTableName . "." . $mainModel->getKeyName(), $type);
		}
		return $baseQuery;
	}

	/**
	 * @param array $attributes
	 * @param string $joinTableName
	 * @return array <String>
	 */
	private static function getAsJoinAttribute($attributes = [], $joinTableName = null): array
	{
		$newAttributes = [];
		foreach ($attributes as $attribute) {
			if (!!strpos($attribute, " as ")) {
				$newAttributes[] = $attribute;
			} else {
				$newAttributes[] = "$attribute as " . implode("_", [$joinTableName, $attribute]);
			}
		}
		return $newAttributes;
	}

	/**
	 * @param Builder $baseQuery
	 * @param Builder $query
	 * @param $as
	 * @param $first
	 * @param $operator
	 * @param $second
	 * @param $type
	 * @return Builder
	 */
	public static function joinQuery(Builder $baseQuery, $query, $as, $first, $operator, $second, $type)
	{
		$baseQuery->join(\DB::raw("(" . $query->toSql() . ") as `$as`"), $first, $operator, $second, $type);
		$baseQuery->addBinding($query->getQuery()->getBindings(), "join");
		return $baseQuery;
	}


	/**
	 * @param Builder $baseQuery
	 * @param $joinClass
	 * @param array $attributes
	 * @param string $type
	 * @throws \Exception
	 */
	public static function asHasOne(Builder $baseQuery, $joinClass, $attributes = [], $type = "left")
	{
		$mainModel = self::getModel($baseQuery);
		$mainTableName = $mainModel->getTable();
		$mainForeignKey = $mainModel->getForeignKey();

		$joinModel = self::getModel($joinClass);
		$joinTableName = $joinModel->getTable();
		$joinForeignKey = $joinModel->getForeignKey();

//		$query = $joinModel->newQuery();
		if (empty($attributes)) {
			$attributes[] = "$joinTableName.*";
			$attributes[] = $joinTableName . ".id as " . $joinForeignKey;
		} elseif (!empty($attributes)) {
			if (!in_array($mainForeignKey, $attributes)) {
				$attributes[] = $mainForeignKey;
			}
		}
		$joinClass->select(self::getAsJoinAttribute($attributes, $joinTableName));
		$asTableName = $joinTableName . Str::random(6);
		$onJoin = "$asTableName." . $joinTableName . "_" . $mainForeignKey;
		self::joinQuery($baseQuery, $joinClass, $asTableName, $onJoin, "=",
			$mainTableName . "." . $joinModel->getKeyName(), $type);
	}

}