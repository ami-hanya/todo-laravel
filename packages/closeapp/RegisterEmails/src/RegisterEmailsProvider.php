<?php

namespace Closeapp\RegisterEmail;

use Illuminate\Support\ServiceProvider;

class RegisterEmailsProvider extends ServiceProvider
{
	/**
	 * Bootstrap the application services.
	 *
	 * @return void
	 */
	public function boot()
	{
		include __DIR__ . '/routes.php';
		$this->loadViewsFrom(__DIR__ . '/views', 'welcome_email');
		$this->loadViewsFrom(__DIR__ . '/views/layouts', 'welcome_email.layouts');
	}

	/**
	 * Register the application services.
	 *
	 * @return void
	 */
	public function register()
	{

	}
}
