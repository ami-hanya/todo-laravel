<?php
/**
 * Created by PhpStorm.
 * User: elad
 * Date: 22/04/18
 * Time: 17:22
 */


use App\User;
use Closeapp\RegisterEmail\WelcomeMail;

Route::get('api/calculator', function () {
	$welcomeMail = new WelcomeMail(User::find(2));
	$welcomeMail->attach(public_path("docs\howToPlay.pdf"), [
		'as' => 'howToPlay.pdf',
		'mime' => 'application/pdf',
	]);
	Mail::to("elad@closeapp.co.il")->send($welcomeMail);
	echo 'Hello from the calculator package!';
});