<?php
/**
 * Created by PhpStorm.
 * User: elad
 * Date: 22/04/18
 * Time: 17:29
 */
?>
@extends('welcome_email.layouts::app')
@section('title')
	<h1 style="font-weight:normal">
		<span>ברוכים הבאים למשחק</span>
		{{$bingo->name}}
	</h1>
@endsection
@section('content')
	<tr>
		<td>הוראות ההפעלה למשחק פשוטות ואנחנו מקווים שתצליחו להעביר את המשחק בקלות.</td>
	</tr>
	<tr>
		<td>
			<span>לשחק במשחק הכנסו לכאן:</span>
			<a href="https://{{$bingo->url}}">https://{{$bingo->url}}/</a>
			<strong>והכניסו את שם המשתמש והססמה.</strong>
		</td>
	</tr>
	<tr>
		<td>
			<span>לפני שאתם מפעילים את המשחק הכנסו לקישור הבא לתרגל את ההפעלה של המשחק:</span>
			<a href="https://{{$bingo->url}}/demo">https://{{$bingo->url}}/demo</a>

		</td>
	</tr>
	<tr>
		<td><p>
				<span lang="he-IL"><span style="font-family: Spacer,serif;"><span style="font-size: medium;">לאחר שהורדתם את הלוחות</span></span></span><span style="font-family: Calibri, serif;"><span style="font-size: medium;">, </span></span><span lang="he-IL"><span style="font-family: Spacer;"><span style="font-size: medium;">גזרתם וחילקתם לכולם טענו את המשחק עצמו</span></span></span><span style="font-family: Calibri, serif;"><span style="font-size: medium;">.</span></span>
			</p>
			<p>
				<span lang="he-IL"><span style="font-family: Spacer,serif;"><span style="font-size: medium;">כשהמשחק עולה לחצו על </span></span></span><span style="font-family: Calibri, serif;"><span style="font-size: medium;"><strong>F11</strong></span> </span><span lang="he-IL"><span style="font-family: Spacer;"><span style="font-size: medium;">והמסך יעבור </span></span><span style="font-family: Spacer;"><span style="font-size: medium;"><strong>למסך מלא</strong></span></span></span><span style="font-family: Calibri, serif;"><span style="font-size: medium;">.</span></span>
			</p>
			<p>
				<span lang="he-IL"><span style="font-family: Spacer,serif;"><span style="font-size: medium;">כשכולם מוכנים יש ללחוץ על </span></span><span style="font-family: Spacer;"><span style="font-size: medium;"><strong>רווח</strong></span></span><span style="font-family: Spacer;"><span style="font-size: medium;"> והמשחק מתחיל</span></span></span><span style="font-family: Calibri, serif;"><span style="font-size: medium;">.</span></span>
			</p>
			<p>
				<span lang="he-IL"><span style="font-family: Spacer,serif;"><span style="font-size: medium;">מעכשיו השתמשו כל הזמן במקש </span></span></span><span style="font-family: Calibri, serif;"><span style="font-size: medium;"><strong>N</strong></span> </span><span lang="he-IL"><span style="font-family: Spacer;"><span style="font-size: medium;">במקלדת</span></span></span><span style="font-family: Calibri, serif;"><span style="font-size: medium;">. </span></span><span lang="he-IL"><span style="font-family: Spacer;"><span style="font-size: medium;">הוא מעביר את המשחק קדימה</span></span></span><span style="font-family: Calibri, serif;"><span style="font-size: medium;">.</span></span>
			</p></td>
	</tr>
	<tr>
		<td><p>
				<span lang="he-IL"><span style="font-family: Spacer,serif;"><span style="font-size: medium;"><em>הקפידו ללחוץ בהתאם למה שקורה במשחק ולקצב של השחקנים</em></span></span></span><span style="font-family: Calibri, serif;"><span style="font-size: medium;"><em>. </em></span></span><span lang="he-IL"><span style="font-family: Spacer;"><span style="font-size: medium;"><em>תנו להם זמן לסמן ולעקוב אחרי השאלות והתשובות</em></span></span></span><span style="font-family: Calibri, serif;"><span style="font-size: medium;"><em>.</em></span></span>
			</p></td>
	</tr>
	<tr>
		<td><p>
				<span lang="he-IL"><span style="font-family: Spacer,serif;"><span style="font-size: medium;"><strong>המקשים השימושיים הם</strong></span></span></span><span style="font-family: Calibri, serif;"><span style="font-size: medium;"><strong>:</strong></span></span>
			</p>
			<p>
				<span style="font-family: Calibri, serif;"><span style="font-size: medium;"><strong>N &ndash; </strong></span></span><span lang="he-IL"><span style="font-family: Spacer;"><span style="font-size: medium;"><strong>מעביר הלאה והוא הכפתור השמיש ביותר</strong></span></span></span><span style="font-family: Calibri, serif;"><span style="font-size: medium;"><strong>.</strong></span></span>
			</p>
			<p>
				<span style="font-family: Calibri, serif;"><span style="font-size: medium;"><strong>P &ndash; </strong></span></span><span lang="he-IL"><span style="font-family: Spacer;"><span style="font-size: medium;"><strong>מחזיר אחורה </strong></span></span></span><span style="font-family: Calibri, serif;"><span style="font-size: medium;"><strong>(</strong></span></span><span lang="he-IL"><span style="font-family: Spacer;"><span style="font-size: medium;"><strong>אם פספסת שאלה או איזה קטע וידאו שלא עבר טוב</strong></span></span></span><span style="font-family: Calibri, serif;"><span style="font-size: medium;"><strong>).</strong></span></span>
			</p>
			<p>
				<span style="font-family: Calibri, serif;"><span style="font-size: medium;"><strong>F11 - </strong></span></span><span lang="he-IL"><span style="font-family: Spacer;"><span style="font-size: medium;"><strong>מעביר את המשחק למסך מלא ומחזיר בסיום למסך רגיל</strong></span></span></span><span style="font-family: Calibri, serif;"><span style="font-size: medium;"><strong>.</strong></span></span>
			</p>
			<p>
				<span style="font-family: Calibri, serif;"><span style="font-size: medium;"><strong>Space (</strong></span></span><span lang="he-IL"><span style="font-family: Spacer;"><span style="font-size: medium;"><strong>מקש רווח</strong></span></span></span><span style="font-family: Calibri, serif;"><span style="font-size: medium;"><strong>) - </strong></span></span><span lang="he-IL"><span style="font-family: Spacer;"><span style="font-size: medium;"><strong>עוצר את הוידאו וממשיך אותו לאחר עצירה</strong></span></span></span><span style="font-family: Calibri, serif;"><span style="font-size: medium;"><strong>.</strong></span></span>
			</p>
			<p>
				<span style="font-family: Calibri, serif;"><span style="font-size: medium;"><strong>M &ndash; </strong></span></span><span lang="he-IL"><span style="font-family: Spacer;"><span style="font-size: medium;"><strong>משתיק את הוידאו</strong></span></span></span><span style="font-family: Calibri, serif;"><span style="font-size: medium;"><strong>.</strong></span></span>
			</p></td>
	</tr>
	<tr>
		<td><p>
				<span lang="he-IL"><span style="font-family: Spacer,serif;"><span style="font-size: medium;"><strong>שימו לב</strong></span></span></span><span style="font-family: Calibri, serif;"><span style="font-size: medium;"><strong>! <br/> 1.</strong></span></span><span lang="he-IL"><span style="font-family: Spacer;"><span style="font-size: medium;"><strong>לאחר </strong></span></span></span><span style="font-family: Calibri, serif;"><span style="font-size: medium;"><strong>5 </strong></span></span><span lang="he-IL"><span style="font-family: Spacer;"><span style="font-size: medium;"><strong>שאלות זה יחשב ששיחקתם במשחק ולא תוכלו לשחק בו שוב ללא תשלום נוסף</strong></span></span></span><span style="font-family: Calibri, serif;"><span style="font-size: medium;"><strong>.<br/> 2.</strong></span></span><span lang="he-IL"><span style="font-family: Spacer;"><span style="font-size: medium;"><strong>השימוש במשחק מחייב חיבור יציב לאינטרנט וזאת כיוון שהסרטונים נטענים מאתר יוטוב </strong></span></span></span><span style="font-family: Calibri, serif;"><span style="font-size: medium;"><strong>((YOUTUBE, </strong></span></span><span lang="he-IL"><span style="font-family: Spacer;"><span style="font-size: medium;"><strong>באם יש לכם חסימה יש לפתוח אותה לפני השימוש</strong></span></span></span><span style="font-family: Calibri, serif;"><span style="font-size: medium;"><strong>.<br/> 3.</strong></span></span><span lang="he-IL"><span style="font-family: Spacer;"><span style="font-size: medium;"><strong>המשחק עובד אך ורק במחשב ולא בטלפונים ניידים</strong></span></span></span><span style="font-family: Calibri, serif;"><span style="font-size: medium;"><strong>.</strong></span></span>
			</p></td>
	</tr>
	<tr>
		<td>נשמח לקבל מכם פידבק על חווית השימוש והמשחק עצמו בקישור הבא:</td>
	</tr>
	<tr>
		<td><a href="https://goo.gl/forms/zhKiWRPBPH2FYRuR2">https://goo.gl/forms/zhKiWRPBPH2FYRuR2</a></td>
	</tr>
	<tr>
		<td><span></span><span></span></td>
	</tr>
	<tr>
		<td>
			<span>מצאתם בעיה? יש לכם שאלה? פנו אלינו במייל</span>
			<span><a href="mailto:office@closeapp.co.il">office@closeapp.co.il</a></span></td>
	</tr>
	<tr>
		<td>
			<span>במקרה חירום אפשר לפנות לטלפון </span><span><strong><a href="tel:0529125251">052-9125251 </a> </strong></span>
		</td>
	</tr>
		<td><strong>בהצלחה</strong></td>
	</tr><tr>

@endsection

