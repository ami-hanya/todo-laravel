<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<body style="direction:rtl; font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; background-color: #f5f8fa; color: #74787E; height: 100%; hyphens: auto; line-height: 1.4; margin: 0; -moz-hyphens: auto; -ms-word-break: break-all; width: 100% !important; -webkit-hyphens: auto; -webkit-text-size-adjust: none; word-break: break-word;">
<style>
	@media only screen and (max-width: 600px) {
		.inner-body {
			width: 100% !important;
		}

		.footer {
			width: 100% !important;
		}
	}

	@media only screen and (max-width: 500px) {
		.button {
			width: 100% !important;
		}
	}
</style>
<table dir="rtl" class="wrapper" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; background-color: #f5f8fa; margin: 0; padding: 0; width: 100%; -premailer-cellpadding: 0; -premailer-cellspacing: 0; -premailer-width: 100%;" width="100%" cellspacing="0" cellpadding="0">
	<tbody>
	<tr>
		<td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;" align="center">
			<table class="content" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; margin: 0; padding: 0; width: 100%; -premailer-cellpadding: 0; -premailer-cellspacing: 0; -premailer-width: 100%;" width="100%" cellspacing="0" cellpadding="0">
				<tbody>
				<tr>
					<td class="header" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; padding: 25px 0; text-align: center;">
						<a style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; color: #bbbfc3; font-size: 19px; font-weight: bold; text-decoration: none; text-shadow: 0 1px 0 white;" href="{{config("app.url")}}">
							{{$bingo->name}}
						</a>
					</td>
				</tr>
				<!-- Email Body -->
				<tr>
					<td class="body" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; background-color: #ffffff; border-bottom: 1px solid #EDEFF2; border-top: 1px solid #EDEFF2; margin: 0; padding: 0; width: 100%; -premailer-cellpadding: 0; -premailer-cellspacing: 0; -premailer-width: 100%;" width="100%">
						<table class="inner-body" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; background-color: #ffffff; margin: 0 auto; padding: 0; width: 570px; -premailer-cellpadding: 0; -premailer-cellspacing: 0; -premailer-width: 570px;" width="570" cellspacing="0" cellpadding="0" align="center"><!-- Body content -->
							<tbody>
							<tr>
								<td class="content-cell" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; padding: 35px; text-align: right;">
									<h3 style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;">
										@yield('title')
									</h3>
									<table style="direction: rtl" class="subcopy" width="100%" cellspacing="0" cellpadding="0">
										<tbody>
										@yield('content')
										</tbody>
									</table>
									<pre style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;"><code></code></pre>
									<p style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; color: #74787e; font-size: 16px; line-height: 1.5em; margin-top: 0; text-align: left;">&nbsp;</p>
								</td>
							</tr>
							</tbody>
						</table>
					</td>
				</tr>
				</tbody>
			</table>
		</td>
	</tr>
	<tr>
		<td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;">
			<table class="footer" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; margin: 0 auto; padding: 0; text-align: center; width: 570px; -premailer-cellpadding: 0; -premailer-cellspacing: 0; -premailer-width: 570px;" width="570" cellspacing="0" cellpadding="0" align="center">
				<tbody>
				<tr>
					<td class="content-cell" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; padding: 35px;" align="center">
						<p style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; line-height: 1.5em; margin-top: 0; color: #aeaeae; font-size: 12px; text-align: center;">&copy; {{$bingo->name}}. All rights reserved.</p>
					</td>
				</tr>
				</tbody>
			</table>
		</td>
	</tr>
	</tbody>
</table>
</body>
</html>