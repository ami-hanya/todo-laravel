<?php

namespace Closeapp\RegisterEmail;

use App\CurrentBingo;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class WelcomeMail extends Mailable
{
	use Queueable, SerializesModels;

	/**
	 * @var User
	 */
	public $user;

	/**
	 * Create a new message instance.
	 *
	 * @param User $user
	 */
	public function __construct(User $user)
	{
//
		$this->user = $user;
	}

	/**
	 * Build the message.
	 *
	 * @return $this
	 */
	public function build()
	{
		$bingo = CurrentBingo::getCurrentBingo();
		return $this->view('welcome_email::email')->with(["bingo" => $bingo])->subject("ברוך הבא ל" . $bingo->name);
	}
}
