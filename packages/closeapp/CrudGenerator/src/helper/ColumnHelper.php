<?php
/**
 * Created by PhpStorm.
 * User: elad
 * Date: 03/07/18
 * Time: 09:31
 */

namespace Closeapp\CrudGenerator\helper;


use stdClass;
use function implode;

class ColumnHelper
{

	/**
	 * @var stdClass
	 */
	private $dataType;
	private $type;

	public function __construct(stdClass $dataType, $type)
	{
		$this->dataType = $dataType;
		$this->type = $type;
	}

	public function getField()
	{
		return $this->dataType->Field;
	}

	public function getFullType()
	{
		return $this->dataType->Type;
	}

	public function getType()
	{
		return $this->type;
	}

	public function isNullAble()
	{
		return $this->dataType->Null === "NO" && $this->getDefault() === null ? false : true;
	}

	public function getKey()
	{
		return $this->dataType->Key;
	}

	public function getExtra()
	{
		return $this->dataType->Extra;
	}

	public function getDefault()
	{
		return $this->dataType->Default;
	}

	public function getValidate()
	{
		$validates = [];
		$validates[] = $this->isNullAble() ? "nullable" : "required";
		if (in_array($this->type, ["integer", "date", "boolean"])) {
			$validates[] = $this->type;
		}
		return implode("|", $validates);
	}

	public static function getTableColumns($table)
	{
		$columnsHelper = [];
		$columns = \DB::select(\DB::raw('SHOW COLUMNS FROM ' . $table));

		foreach ($columns as $column) {
			$colName = $column->Field;
			$type = \DB::getSchemaBuilder()->getColumnType($table, $colName);
			$columnsHelper[] = new ColumnHelper($column, $type);
		}
		return $columnsHelper;
	}

}