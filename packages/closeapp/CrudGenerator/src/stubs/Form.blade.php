<?php
/**
 * Created by PhpStorm.
 * User: elad
 * Date: 11/06/18
 * Time: 12:13
 */
?>

<!--suppress ALL -->
<!--suppress UnreachableCodeJS -->
<template>

	<form @submit.prevent="onSubmit">

		@foreach($validations as $key => $validation)
			<smart-input :error="inputErrors.{{$key}}" label='{{$key}}' v-model="form.{{$key}}"></smart-input>
		@endforeach
			<div class="form-group">
				<button type="submit" class="btn btn-primary">שמירה</button>
				<button type="button" v-if="!isNew" @click="onReset" class="btn btn-danger">חדש</button>
			</div>
	</form>
</template>


<script>
	import {types} from "dev/store/modules/{{$modelName}}.module";
	import SmartInput from "dev/components/SmartInput";
	import FormView from "dev/mixins/FormView";
	export default {
		props: [],
		name: "{{$modelNameSingularLowerCase}}-form",
		components: { SmartInput},
		data() {
			return {
				form: {
					name: null,
				},
			};
		},
		mixins: [new FormView(types)],
		computed: {
				{{$modelNameSingularLowerCase}}() {
				return this.$store.state.{{$modelName}}Module.{{$modelNameSingularLowerCase}};
			},
			isNew(){
				return !!this.{{$modelNameSingularLowerCase}}.id;
			}
		},
		watch: {
			{{$modelNameSingularLowerCase}}() {
				this.form = Object.assign({}, this.{{$modelNameSingularLowerCase}});
				this.id = this.form.id;
			}
		}
	};
</script>

<style>

</style>