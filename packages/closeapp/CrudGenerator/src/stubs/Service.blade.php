<?php
/**
 * Created by PhpStorm.
 * User: elad
 * Date: 11/06/18
 * Time: 12:13
 */
?>


import BaseService from "./BaseService";

export default class {{$modelName}}LessonService extends BaseService {
	static get className() {
		return "{{$modelNameSingularLowerCase}}";
	}
};