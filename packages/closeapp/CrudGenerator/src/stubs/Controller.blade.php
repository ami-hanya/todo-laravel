<?php
/**
 * Created by PhpStorm.
 * User: elad
 * Date: 11/06/18
 * Time: 12:13
 */
?>

namespace App\Http\Controllers;

@if($namespace)
	namespace App\Http\Controllers\{{$namespace}};
	use App\Http\Controllers\Controller;
@else
	namespace App\Http\Controllers;
@endif;

use App\Http\Requests\{{$modelName}}Request;
use App\{{$modelName}};


class {{$modelName}}Controller extends Controller
{
	public function index()
	{
		${{$modelNamePluralLowerCase}} = {{$modelName}}::query()->paginate();

		return response()->json(${{$modelNamePluralLowerCase}});
	}

	public function store({{$modelName}}Request $request)
	{
		${{$modelNameSingularLowerCase}} = {{$modelName}}::create($request->validated());

		return response()->json(${{$modelNameSingularLowerCase}});
	}

	public function show($id)
	{
		${{$modelNameSingularLowerCase}} = {{$modelName}}::findOrFail($id);

		return response()->json(${{$modelNameSingularLowerCase}});
	}

	public function update({{$modelName}}Request $request, $id)
	{
		${{$modelNameSingularLowerCase}} = {{$modelName}}::findOrFail($id);
		${{$modelNameSingularLowerCase}}->update($request->all());

		return response()->json(${{$modelNameSingularLowerCase}}, 200);
	}

	public function destroy($id)
	{
		${{$modelNameSingularLowerCase}} = {{$modelName}}::findOrFail($id);
		${{$modelNameSingularLowerCase}}->delete();


		return response()->json(${{$modelNameSingularLowerCase}},200);
	}
}
