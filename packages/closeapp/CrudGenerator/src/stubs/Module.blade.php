<?php
/**
 * Created by PhpStorm.
 * User: elad
 * Date: 11/06/18
 * Time: 12:13
 */
?>


import {{$modelName}}Service from "dev/services/{{$modelName}}Service";
import ModuleCreator from "dev/store/ModuleCreator";

export const types = {};

/**
@namespace
@property {object} module,
@property {object} module.state,
@property {object} modules.{{$modelNameSingularLowerCase}},
@property {array}  modules.{{$modelNamePluralLowerCase}}
**/

const module = {
	state: {},
	mutations: {},
	getters: {},
	actions: {},
	types: types
};
export default ModuleCreator("{{$modelNameSingularLowerCase}}", {{$modelName}}Service, module, types);