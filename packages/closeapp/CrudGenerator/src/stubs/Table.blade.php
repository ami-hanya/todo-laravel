<?php
/**
 * Created by PhpStorm.
 * User: elad
 * Date: 11/06/18
 * Time: 12:13
 */
?>

<template>
	<v-data-table
			:headers="headers"
			:items="{{$modelNamePluralLowerCase}}"
			:loading="loading"
			class="elevation-1"
	>
		<v-progress-linear slot="progress" color="blue" indeterminate></v-progress-linear>
		<bingo-row slot="items" slot-scope="props" :bingo="props.item" @edit="openDialog"></bingo-row>
		<{{$modelNameSingularLowerCase}}-row slot="items" slot-scope="props" @edit="openDialog" :{{$modelNameSingularLowerCase}}="props.item" @edit="openDialog"/>
	</v-data-table>
</template>

<script>
	import {{$modelName}}Row from "dev/view/{{$modelNamePluralLowerCase}}/{{$modelName}}Row";
	import {types} from "dev/store/modules/{{$modelName}}.module";
	import FilterAble from "dev/mixins/FilterAble";

	export default {
		name: "{{$modelNamePluralLowerCase}}-table",
		props: [],
		components: {
			{{$modelName}}Row
		},
		mixins: [new FilterAble(types)],
		data() {
			return {loading: false,
				headers: [
			@foreach($validations as $key => $validation)
					{text:"{{$key}}",value:"{{$key}}" },
				@endforeach

				],
				search: null,
				showDialog: false};
		},
		computed: {
			{{$modelNamePluralLowerCase}}() {
				return this.$store.state.{{$modelName}}Module.{{$modelNamePluralLowerCase}};
			}
		},
		methods: {},
		created() {
		},
		mounted() {
		},
		updated() {
		},
		destroyed() {
		}
	};
</script>

<style>

</style>