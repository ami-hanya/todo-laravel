<?php
/**
 * Created by PhpStorm.
 * User: elad
 * Date: 11/06/18
 * Time: 12:13
 */
?>

<template>

	<page>
		<row>
			<c-col>
				<title-view>מסך {{$modelNamePluralLowerCase}}</title-view>
				<{{$modelNameSingularLowerCase}}-form/>

			</c-col>
			<c-col>
				<{{$modelNamePluralLowerCase}}-table/>
			</c-col>
		</row>

	</page>


</template>

<script>
	import {{$modelName}}Form from "dev/view/";
	import {{$modelNamePlural}}Table from "dev/view/";

	export default {
		name: "{{$modelNamePluralLowerCase}}-view",
		components: {{"{".$modelNamePlural}}Table, {{$modelName}}Form},
	};
</script>

<style scoped>

</style>