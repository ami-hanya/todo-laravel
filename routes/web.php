<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['prefix' => 'api'], function () {
	Route::post('auth', 'Auth\RegisterController@register');
	Route::get("auth", "Admin\LoginController@get");
	Route::post("login", "Auth\LoginController@login");
	Route::get("logout", "Admin\LoginController@logout");
	Route::group(['middleware' => 'auth'], function () {
		Route::resource("tasks", "TaskController");
	});
});


Route::get('/', 'HomeController@index')->name('index');
