<p align="center"><img src="https://laravel.com/assets/img/components/logo-laravel.svg"></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

## TODO API

## Build

1. run - composer install
2. create DataBase (collection: utfmb8 general-ci)
3. copy paste .env-example to .env (don't remove env-example!)
4. הכנס את פרטי המסד נתונים לקובץ הenv
5. run php artisan migrate - crate database tables
6. crate virtual host to the public directory
7. start to work!

The index file is index.blade.php
all yours files need to be in the public directory


##### AUTH API
* <b>register</b> - POST  api/auth 
* <b>get user login</b> - GET  api/auth 
* <b>do login</b> - POST  api/login 
* <b>do logout</b> - GET  api/logout 

##### Tasks API
* <b>create</b> - POST  api/tasks (params:title,is_done[0,1])
* <b>read</b> - GET  api/tasks
* <b>update</b> - PUT  api/tasks/:id (params:title,is_done[0,1])
* <b>delete</b> - DELETE  api/tasks/:id
