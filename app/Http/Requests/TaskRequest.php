<?php
namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TaskRequest extends FormRequest
{
	public function authorize()
	{
		return true;
	}

	public function rules()
	{
		return [
					"title"=>"required",
					"is_done"=>"nullable|boolean",
				];
	}

	public function validated()
	{
		return parent::validated(); // TODO: Change the autogenerated stub
	}

}
