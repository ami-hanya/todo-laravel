<?php

namespace App\Http\Controllers;

namespace App\Http\Controllers;

;

use App\Http\Requests\TaskRequest;
use App\Task;
use Illuminate\Support\Facades\Auth;
use function abort;


class TaskController extends Controller
{
	public function index()
	{
		$tasks = Task::query()->where("user_id", Auth::id())->paginate();
		return response()->json($tasks);
	}

	public function store(TaskRequest $request)
	{
		$task = Task::create($request->validated());
		$task->user_id = Auth::id();
		$task->is_done = 0;
		return response()->json($task);
	}

	public function show($id)
	{
		$task = Task::findOrFail($id);
		if ($task->user_id != Auth::id()) {
			abort(401);
		}
		return response()->json($task);
	}

	public function update(TaskRequest $request, $id)
	{
		$task = Task::findOrFail($id);
		if ($task->user_id != Auth::id()) {
			abort(401);
		}
		$task->update($request->all());

		return response()->json($task, 200);
	}

	public function destroy($id)
	{
		$task = Task::findOrFail($id);
		if ($task->user_id != Auth::id()) {
			abort(401);
		}
		$task->delete();
		return response()->json($task, 200);
	}
}
